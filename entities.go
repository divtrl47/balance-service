package balance_service

import "fmt"

type Asset struct {
	ID   uint
	Name string
	Code string
}

type Account struct {
	ID   uint
	Name string
}

type Balance struct {
	ID        uint
	AccountID uint

	Account *Account

	Asset        string
	Amount       float64
	FrozenAmount float64
}

var ErrBalanceNotFound = fmt.Errorf("balance not found")
var ErrAccountNotFound = fmt.Errorf("account not found")
var ErrNotEnoughBalance = fmt.Errorf("not enough balance")
