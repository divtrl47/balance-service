package gorm

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"gorm.io/gorm"
)

type Account struct {
	gorm.Model
	Name string `gorm:"name"`
}

type AccountRepo struct {
	db *gorm.DB
}

func NewAccountRepo(db *gorm.DB) *AccountRepo {
	return &AccountRepo{db: db}
}

func (a *AccountRepo) Append(account billing.Account) (uint, error) {
	m := &Account{
		Name: account.Name,
	}
	result := a.db.Create(m)
	return m.ID, result.Error
}

func (a *AccountRepo) Update(account billing.Account) error {
	m := &Account{}
	m.ID = account.ID
	a.db.First(m, account.ID)

	m.Name = account.Name

	return a.db.Save(m).Error
}

func (a *AccountRepo) Get(id uint) (billing.Account, error) {
	m := &Account{}
	tx := a.db.First(m, id)

	return billing.Account{
		ID:   m.ID,
		Name: m.Name,
	}, tx.Error
}

func (a *AccountRepo) Find(filter billing.Account) (billing.Account, error) {
	m := &Account{}
	tx := a.db.First(m, "name = ?", filter.Name)
	if tx.Error != nil {
		return billing.Account{}, tx.Error
	}

	return billing.Account{
		ID:   m.ID,
		Name: m.Name,
	}, nil
}

func (a *AccountRepo) Delete(accountID uint) error {
	return a.db.Delete(&Account{}, accountID).Error
}
