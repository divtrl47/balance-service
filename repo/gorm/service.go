package gorm

import (
	"fmt"
	"gorm.io/gorm"
)

func New(db *gorm.DB) (*gorm.DB, error) {
	err := db.AutoMigrate(
		&Account{},
		&Balance{},
	)
	if err != nil {
		return nil, fmt.Errorf("auto migrate: %v", err)
	}

	return db, err
}
