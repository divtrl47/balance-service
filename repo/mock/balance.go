package mock

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"fmt"
)

type BalanceRepo struct {
	counter uint
	data    map[string]billing.Balance
}

func NewBalanceRepo() *BalanceRepo {
	return &BalanceRepo{data: map[string]billing.Balance{}}
}

func (a *BalanceRepo) Append(balance billing.Balance) (uint, error) {
	a.counter++
	balance.ID = a.counter
	a.data[a.makeKey(balance.AccountID, balance.Asset)] = balance

	return a.counter, nil
}

func (a *BalanceRepo) Update(balance billing.Balance) error {
	key := a.makeKey(balance.AccountID, balance.Asset)
	if _, ok := a.data[key]; !ok {
		return fmt.Errorf("balance does not exist")
	}

	a.data[key] = balance

	return nil
}

// Find can find only by ID
func (a *BalanceRepo) Find(filter billing.Balance) (billing.Balance, error) {
	key := a.makeKey(filter.AccountID, filter.Asset)
	if balance, ok := a.data[key]; ok {
		return balance, nil
	}

	return billing.Balance{}, billing.ErrBalanceNotFound
}

func (a *BalanceRepo) Delete(balanceID uint) error {
	for code, balance := range a.data {
		if balance.ID == balanceID {
			delete(a.data, code)
			return nil
		}
	}

	return fmt.Errorf("balance does not exist")
}

func (a *BalanceRepo) makeKey(accountID uint, asset string) string {
	return fmt.Sprintf("%d-%s", accountID, asset)
}
