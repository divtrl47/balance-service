module bitbucket.org/divtrl47/balance-service

go 1.15

require (
	github.com/nats-io/nats.go v1.11.0
	github.com/sirupsen/logrus v1.4.2
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.9
)
