package balancer

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"fmt"
	"sync"
)

type BalanceRepo interface {
	Append(Balance billing.Balance) (uint, error)
	Update(Balance billing.Balance) error
	Find(filter billing.Balance) (billing.Balance, error)
	Delete(assetID uint) error
}

type BalanceService struct {
	balances BalanceRepo
	accounts AccountRepo

	lockers sync.Map
}

func NewBalanceService(balances BalanceRepo, accounts AccountRepo) *BalanceService {
	return &BalanceService{balances: balances, accounts: accounts}
}

func (service *BalanceService) createBalance(accountID uint, asset string, amount float64) error {
	_, err := service.balances.Append(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
		Amount:    amount,
	})
	if err != nil {
		return fmt.Errorf("cound not create balance: %v", err)
	}

	return nil
}

func (service *BalanceService) Debit(accountID uint, asset string, amount float64) error {
	mu := service.locker(accountID)
	mu.Lock()
	defer mu.Unlock()

	_, err := service.accounts.Get(accountID)
	if err != nil {
		return fmt.Errorf("account not found: %v", err)
	}

	balance, err := service.balances.Find(billing.Balance{
		AccountID: accountID,
		Asset:     asset,
	})
	if err != nil {
		return err
	}

	if amount > balance.Amount {
		return billing.ErrNotEnoughBalance
	}

	balance.Amount -= amount

	return service.balances.Update(balance)
}

func (service *BalanceService) TopUp(accountID uint, asset string, amount float64) error {
	mu := service.locker(accountID)
	mu.Lock()
	defer mu.Unlock()

	_, err := service.accounts.Get(accountID)
	if err != nil {
		return fmt.Errorf("account not found: %v", err)
	}

	balance, err := service.balances.Find(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
	})
	if err != nil {
		return service.createBalance(accountID, asset, amount)
	}

	balance.Amount += amount

	return service.balances.Update(balance)
}

func (service *BalanceService) Freeze(accountID uint, asset string, amount float64) error {
	mu := service.locker(accountID)
	mu.Lock()
	defer mu.Unlock()

	_, err := service.accounts.Get(accountID)
	if err != nil {
		return fmt.Errorf("account not found: %v", err)
	}

	balance, err := service.balances.Find(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
	})
	if err != nil {
		return err
	}

	if amount > balance.Amount {
		return fmt.Errorf("debit too large")
	}

	balance.Amount -= amount
	balance.FrozenAmount += amount

	return service.balances.Update(balance)
}

func (service *BalanceService) DebitFrozen(accountID uint, asset string, amount float64) error {
	mu := service.locker(accountID)
	mu.Lock()
	defer mu.Unlock()

	_, err := service.accounts.Get(accountID)
	if err != nil {
		return fmt.Errorf("account not found: %v", err)
	}

	balance, err := service.balances.Find(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
	})
	if err != nil {
		return err
	}

	if amount > balance.FrozenAmount {
		return fmt.Errorf("not enough frozen amount on account")
	}

	balance.FrozenAmount -= amount

	return service.balances.Update(balance)
}

func (service *BalanceService) Unfreeze(accountID uint, asset string, amount float64) error {
	mu := service.locker(accountID)
	mu.Lock()
	defer mu.Unlock()

	_, err := service.accounts.Get(accountID)
	if err != nil {
		return fmt.Errorf("account not found: %v", err)
	}

	balance, err := service.balances.Find(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
	})
	if err != nil {
		return err
	}

	if amount > balance.FrozenAmount {
		return fmt.Errorf("frozen amount too large")
	}

	balance.FrozenAmount -= amount
	balance.Amount += amount

	return service.balances.Update(balance)
}

func (service *BalanceService) Transfer(accountFromID, accountToID uint, asset string, amount float64) error {
	err := service.Debit(accountFromID, asset, amount)
	if err != nil {
		return err
	}

	err = service.TopUp(accountToID, asset, amount)
	if err != nil {
		err = service.TopUp(accountFromID, asset, amount)
		if err != nil {
			return fmt.Errorf("could not recover source balance after failed transfer: %v", err)
		}
		return err
	}

	return nil
}

func (service *BalanceService) Balance(accountID uint, asset string) (billing.Balance, error) {
	return service.balances.Find(billing.Balance{
		Asset:     asset,
		AccountID: accountID,
	})
}

func (service *BalanceService) locker(accountID uint) *sync.Mutex {
	if v, ok := service.lockers.Load(accountID); ok {
		return v.(*sync.Mutex)
	}

	mu := &sync.Mutex{}
	service.lockers.Store(accountID, mu)

	return mu
}
