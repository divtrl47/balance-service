.PHONY: demo

demo:
	go mod vendor
	docker-compose rm -f
	docker-compose -f demo.docker-compose.yml build
	docker-compose  -f demo.docker-compose.yml up --force-recreate

test:
	go test -race ./...