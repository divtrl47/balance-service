package balance_service

type DebitRequest struct {
	AccountID uint
	Asset     string
	Amount    float64
}

type DebitFrozenRequest struct {
	AccountID uint
	Asset     string
	Amount    float64
}

type NewAccountRequest struct {
	Name string
}

type DeleteAccountRequest struct {
	ID uint
}

type FreezeRequest struct {
	AccountID uint
	Asset     string
	Amount    float64
}

type UnfreezeRequest struct {
	AccountID uint
	Asset     string
	Amount    float64
}

type TopUpRequest struct {
	AccountID uint
	Asset     string
	Amount    float64
}

type TransferRequest struct {
	AccountFromID uint
	AccountToID   uint
	Asset         string
	Amount        float64
}

type EventType string

const (
	EvenBalanceDebit       = "balance.debit"
	EvenBalanceDebitFrozen = "balance.debit_frozen"
	EvenBalanceTopUp       = "balance.top_up"
	EvenBalanceFreeze      = "balance.freeze"
	EvenBalanceUnfreeze    = "balance.unfreeze"
	EvenBalanceTransfer    = "balance.transfer"

	EventAccountCreate = "account.create"
	EventAccountUpdate = "account.update"
	EventAccountDelete = "account.delete"
)

type UniEvent struct {
	Type    EventType
	Err     string
	Data    interface{}
	Success bool

	// в реальном мире стоило бы сделать отдельную структуру на каждое событие
	Account Account
}
