package main

import (
	bs "bitbucket.org/divtrl47/balance-service"
	"bitbucket.org/divtrl47/balance-service/balancer"
	rg "bitbucket.org/divtrl47/balance-service/repo/gorm"
	"fmt"
	"github.com/nats-io/nats.go"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"os"
)

func main() {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Shanghai",
		os.Getenv("DB_HOST"),
		os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"),
		os.Getenv("POSTGRES_DB"),
		os.Getenv("DB_PORT"),
	)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db, err = rg.New(db)
	if err != nil {
		panic(err)
	}

	nc, err := nats.Connect(fmt.Sprintf("%s:%s", os.Getenv("NATS_HOST"), os.Getenv("NATS_PORT")))
	if err != nil {
		panic(err)
	}

	encodedConn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}

	accountRepo := rg.NewAccountRepo(db)
	balanceRepo := rg.NewBalanceRepo(db)

	balanceService := balancer.NewBalanceService(balanceRepo, accountRepo)
	accountService := balancer.NewAccountService(accountRepo)

	eventCh := make(chan interface{}, 100)
	_, err = encodedConn.Subscribe("billing.balance.debit", func(subj string, req bs.DebitRequest) {
		err = balanceService.Debit(req.AccountID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceDebit,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.balance.freeze", func(subj string, req bs.FreezeRequest) {
		err = balanceService.Freeze(req.AccountID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceFreeze,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.balance.unfreeze", func(subj string, req bs.UnfreezeRequest) {
		err = balanceService.Unfreeze(req.AccountID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceUnfreeze,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.balance.topUp", func(subj string, req bs.TopUpRequest) {
		err = balanceService.TopUp(req.AccountID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceTopUp,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.balance.transfer", func(subj string, req bs.TransferRequest) {
		err = balanceService.Transfer(req.AccountFromID, req.AccountToID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceTransfer,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.balance.debit_frozen", func(subj string, req bs.DebitRequest) {
		err = balanceService.DebitFrozen(req.AccountID, req.Asset, req.Amount)

		eventCh <- bs.UniEvent{
			Type:    bs.EvenBalanceDebitFrozen,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.account.create", func(subj string, req bs.NewAccountRequest) {
		var id uint
		id, err = accountService.Create(bs.Account{
			Name: req.Name,
		})
		if err != nil {
			return
		}

		eventCh <- bs.UniEvent{
			Account: bs.Account{
				ID:   id,
				Name: req.Name,
			},
			Type:    bs.EventAccountCreate,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.account.delete", func(subj string, req bs.DeleteAccountRequest) {
		err = accountService.Delete(req.ID)
		if err != nil {
			return
		}

		eventCh <- bs.UniEvent{
			Type:    bs.EventAccountDelete,
			Err:     fmt.Sprint(err),
			Data:    req,
			Success: err == nil,
		}
	})
	if err != nil {
		panic(err)
	}

	for event := range eventCh {
		err = encodedConn.Publish("billing.events", event)
		if err != nil {
			panic(err)
		}
	}
}
