package balancer

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"bitbucket.org/divtrl47/balance-service/repo/mock"
	"testing"
)

var bobID uint
var aliceID uint

const assetUSD = "usd"

func TestMain(m *testing.M) {
	m.Run()
}

func TestBalanceService_Balance(t *testing.T) {
	balanceService := newBalanceService()

	_, err := balanceService.Balance(bobID, "usd")
	if err != billing.ErrBalanceNotFound {
		t.Fatalf("expect %v, got %v", billing.ErrBalanceNotFound, err)
	}
}

func TestBalanceService_DebitFrozen(t *testing.T) {
	balanceService := newBalanceService()

	err := balanceService.TopUp(bobID, assetUSD, 100)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	err = balanceService.Freeze(bobID, assetUSD, 90)
	if err != nil {
		t.Fatalf("could not freeze: %v", err)
	}

	err = balanceService.Transfer(bobID, aliceID, assetUSD, 90)
	if err != billing.ErrNotEnoughBalance {
		t.Fatalf("unfrozen balance: %v", err)
	}

	err = balanceService.DebitFrozen(bobID, assetUSD, 90)
	if err != nil {
		t.Fatalf("could not debit frozen balance: %v", err)
	}
}

func TestBalanceService_TopUp(t *testing.T) {
	balanceService := newBalanceService()

	err := balanceService.TopUp(bobID, assetUSD, 100)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	err = balanceService.TopUp(aliceID, assetUSD, 100)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	balance, err := balanceService.Balance(aliceID, assetUSD)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if balance.Amount != 100 {
		t.Fatalf("unexpected balance 100, got %v", balance.Amount)
	}

	balance, err = balanceService.Balance(bobID, assetUSD)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if balance.Amount != 100 {
		t.Fatalf("unexpected balance 100, got %v", balance.Amount)
	}
}

func newBalanceService() *BalanceService {
	accountRepo := mock.NewAccountRepo()
	balanceRepo := mock.NewBalanceRepo()

	var err error
	bobID, err = accountRepo.Append(billing.Account{
		Name: "Bob",
	})
	if err != nil {
		panic(err)
	}

	aliceID, err = accountRepo.Append(billing.Account{
		Name: "Alice",
	})
	if err != nil {
		panic(err)
	}

	return NewBalanceService(
		balanceRepo,
		accountRepo,
	)
}

func TestBalanceService_Transfer(t *testing.T) {
	balanceService := newBalanceService()

	err := balanceService.TopUp(bobID, assetUSD, 100)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	err = balanceService.Transfer(bobID, aliceID, assetUSD, 80)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}

	balance, err := balanceService.Balance(aliceID, assetUSD)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if balance.Amount != 80 {
		t.Fatalf("unexpected balance 80, got %v", balance.Amount)
	}

	balance, err = balanceService.Balance(bobID, assetUSD)
	if err != nil {
		t.Fatalf("unexpected error: %v", err)
	}
	if balance.Amount != 20 {
		t.Fatalf("unexpected balance 20, got %v", balance.Amount)
	}
}
