package mock

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"fmt"
)

type AccountRepo struct {
	counter uint
	data    map[uint]billing.Account
}

func NewAccountRepo() *AccountRepo {
	return &AccountRepo{data: map[uint]billing.Account{}}
}

func (a *AccountRepo) Append(account billing.Account) (uint, error) {
	a.counter++
	account.ID = a.counter
	a.data[a.counter] = account

	return a.counter, nil
}

func (a *AccountRepo) Update(account billing.Account) error {
	if _, ok := a.data[account.ID]; !ok {
		return fmt.Errorf("account does not exist")
	}

	a.data[account.ID] = account

	return nil
}

// Find can find only by ID
func (a *AccountRepo) Find(filter billing.Account) (billing.Account, error) {
	if account, ok := a.data[filter.ID]; ok {
		return account, nil
	}

	return billing.Account{}, billing.ErrAccountNotFound
}

// Find can find only by ID
func (a *AccountRepo) Get(id uint) (billing.Account, error) {
	if account, ok := a.data[id]; ok {
		return account, nil
	}

	return billing.Account{}, billing.ErrAccountNotFound
}

func (a *AccountRepo) Delete(accountID uint) error {
	if _, ok := a.data[accountID]; !ok {
		delete(a.data, accountID)
	}

	return fmt.Errorf("account does not exist")
}
