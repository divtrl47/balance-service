package balancer

import billing "bitbucket.org/divtrl47/balance-service"

type AccountRepo interface {
	Append(account billing.Account) (uint, error)
	Update(account billing.Account) error
	Find(filter billing.Account) (billing.Account, error)
	Get(id uint) (billing.Account, error)
	Delete(accountID uint) error
}

type AccountService struct {
	accounts AccountRepo
}

func NewAccountService(accounts AccountRepo) *AccountService {
	return &AccountService{accounts: accounts}
}

func (service *AccountService) Create(Account billing.Account) (uint, error) {
	return service.accounts.Append(Account)
}

func (service *AccountService) Account(name string) (billing.Account, error) {
	return service.accounts.Find(billing.Account{
		Name: name,
	})
}

func (service *AccountService) Delete(accountID uint) error {
	return service.accounts.Delete(accountID)
}
