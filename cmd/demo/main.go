package main

import (
	bs "bitbucket.org/divtrl47/balance-service"
	"fmt"
	"github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
	"os"
	"sync"
)

const (
	Account1 = "Терентьев Михал Палыч"
	Account2 = "КПМ"
)

func main() {
	natsUrl := fmt.Sprintf("%s:%s", os.Getenv("NATS_HOST"), os.Getenv("NATS_PORT"))
	nc, err := nats.Connect(natsUrl)
	if err != nil {
		panic(fmt.Errorf("with nats url %s: %v", natsUrl, err))
	}

	encodedConn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}

	accounts := map[string]uint{}
	accountsMu := sync.Mutex{}
	accountsWG := sync.WaitGroup{}
	wg := sync.WaitGroup{}

	accountsWG.Add(1)
	err = encodedConn.Publish("billing.account.create", bs.NewAccountRequest{
		Name: Account1,
	})
	if err != nil {
		panic(err)
	}

	accountsWG.Add(1)
	err = encodedConn.Publish("billing.account.create", bs.NewAccountRequest{
		Name: Account2,
	})
	if err != nil {
		panic(err)
	}

	_, err = encodedConn.Subscribe("billing.events", func(event bs.UniEvent) {
		switch event.Type {
		case bs.EventAccountCreate:
			if !event.Success {
				panic(fmt.Errorf("could not create account %s: %v", event.Account.Name, event.Err))
			}

			accountsMu.Lock()
			accounts[event.Account.Name] = event.Account.ID
			accountsMu.Unlock()

			accountsWG.Done()

			logrus.WithError(fmt.Errorf(event.Err)).
				WithField("type", event.Type).
				WithField("data", event.Data).
				WithField("success", event.Success).
				Info("account created")

		default:
			wg.Done()
			logrus.WithError(fmt.Errorf(event.Err)).
				WithField("type", event.Type).
				WithField("data", event.Data).
				WithField("success", event.Success).
				Info("new event")
		}
	})
	if err != nil {
		panic(err)
	}

	accountsWG.Wait()

	wg.Add(1)
	err = encodedConn.Publish("billing.balance.topUp", bs.TopUpRequest{
		AccountID: accounts[Account1],
		Asset:     "RUB",
		Amount:    1000000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
	wg.Add(1)
	err = encodedConn.Publish("billing.balance.freeze", bs.FreezeRequest{
		AccountID: accounts[Account1],
		Asset:     "RUB",
		Amount:    500000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
	wg.Add(1)
	err = encodedConn.Publish("billing.balance.unfreeze", bs.UnfreezeRequest{
		AccountID: accounts[Account1],
		Asset:     "RUB",
		Amount:    500000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
	wg.Add(1)
	err = encodedConn.Publish("billing.balance.transfer", bs.TransferRequest{
		AccountFromID: accounts[Account1],
		AccountToID:   accounts[Account2],
		Asset:         "RUB",
		Amount:        900000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
	wg.Add(1)
	err = encodedConn.Publish("billing.balance.freeze", bs.FreezeRequest{
		AccountID: accounts[Account2],
		Asset:     "RUB",
		Amount:    900000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
	wg.Add(1)
	err = encodedConn.Publish("billing.balance.transfer", bs.TransferRequest{
		AccountFromID: accounts[Account2],
		AccountToID:   accounts[Account1],
		Asset:         "RUB",
		Amount:        900000,
	})
	if err != nil {
		panic(err)
	}

	wg.Wait()
}
