package gorm

import (
	billing "bitbucket.org/divtrl47/balance-service"
	"gorm.io/gorm"
)

type Balance struct {
	gorm.Model

	Amount       float64 `gorm:"amount"`
	FrozenAmount float64 `gorm:"frozen_amount"`
	Asset        string  `gorm:"asset"`

	AccountID uint    `gorm:"account_id"`
	Account   Account `gorm:"account"`
}

type BalanceRepo struct {
	db *gorm.DB
}

func NewBalanceRepo(db *gorm.DB) *BalanceRepo {
	return &BalanceRepo{db: db}
}

func (a *BalanceRepo) Append(balance billing.Balance) (uint, error) {
	m := &Balance{
		Amount:       balance.Amount,
		FrozenAmount: balance.FrozenAmount,
		Asset:        balance.Asset,
		AccountID:    balance.AccountID,
	}
	result := a.db.Create(m)
	return m.ID, result.Error
}

func (a *BalanceRepo) Update(balance billing.Balance) error {
	m := &Balance{}
	m.ID = balance.ID
	a.db.First(m, balance.ID)

	m.Asset = balance.Asset
	m.Amount = balance.Amount
	m.FrozenAmount = balance.FrozenAmount

	return a.db.Save(m).Error
}

func (a *BalanceRepo) Find(filter billing.Balance) (billing.Balance, error) {
	m := &Balance{}
	tx := a.db.First(m, "asset = ? and account_id = ?", filter.Asset, filter.AccountID)
	if tx.Error != nil {
		return billing.Balance{}, tx.Error
	}

	return billing.Balance{
		ID:        m.ID,
		AccountID: m.AccountID,
		Account: &billing.Account{
			ID:   m.Account.ID,
			Name: m.Account.Name,
		},
		Asset:        m.Asset,
		Amount:       m.Amount,
		FrozenAmount: m.FrozenAmount,
	}, nil
}

func (a *BalanceRepo) Delete(id uint) error {
	return a.db.Delete(&Balance{}, id).Error
}
